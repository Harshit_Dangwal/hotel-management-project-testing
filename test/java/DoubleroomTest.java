import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DoubleroomTest {
    Doubleroom customer;

    @Before
    public void createInstance() {
        customer = new Doubleroom("raj", "12344321", "male", "rajesh", "13456789", "male");
    }

    @Test
    public void shouldContainAllDetails() {
        assertEquals("raj", customer.name, "name mismatched");
        assertEquals("rajesh", customer.name2, "name2 mismatched");
        assertEquals("12344321", customer.contact, "contact mismatched");
        assertEquals("13456789", customer.contact2, "contact2 mismatched");
        assertEquals("male", customer.gender, "gender mismatched");
        assertEquals("male", customer.gender2, "gender mismatched");
        System.out.println("DoubleroomTest");
    }

    @Test
    public void NotEmptyDetails() {
        assertFalse(customer.name.isEmpty());
        assertFalse(customer.name2.isEmpty());
        assertFalse(customer.contact.isEmpty());
        assertFalse(customer.contact2.isEmpty());
        assertFalse(customer.gender.isEmpty());
        assertFalse(customer.gender2.isEmpty());
    }
    @Test
    public void detailsShouldNotContainNull() {
        assertNotNull(customer.name);
        assertNotNull(customer.name2);
        assertNotNull(customer.contact);
        assertNotNull(customer.contact2);
        assertNotNull(customer.gender);
        assertNotNull(customer.gender2);

    }

}