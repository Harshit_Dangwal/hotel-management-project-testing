import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class FoodTest {
    Food customer;

    @Test
    @Before
    public void createInstance() {
        customer = new Food(1, 1);
    }

    @Test
    public void itemNoShouldLessThenFive() {

        assertTrue(customer.itemno < 5, "itemno should less then five");
    }

    @Test
    @DisplayName("setting final price")
    public void finalFoodPrice() {
        assertEquals(50, customer.price);
        System.out.println("FoodTest");
    }

    @Test
    public void DetailsShouldMinOne() {
        assertTrue(customer.itemno > 0);
        assertTrue(customer.quantity > 0);
    }
}