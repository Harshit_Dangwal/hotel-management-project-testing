import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;


public class HotelAndBookRoomTest {

    @Test
    public void customerDetailsForLuxuryDoubleRoom() throws Exception {
        Hotel.bookroom(1);
        assertEquals("John", Hotel.hotel_ob.luxury_doublerrom[0].name);
        assertEquals("123", Hotel.hotel_ob.luxury_doublerrom[0].contact);
        assertEquals("male", Hotel.hotel_ob.luxury_doublerrom[0].gender);
        assertEquals("Henry", Hotel.hotel_ob.luxury_doublerrom[0].name2);
        assertEquals("1234", Hotel.hotel_ob.luxury_doublerrom[0].contact2);
        assertEquals("male", Hotel.hotel_ob.luxury_doublerrom[0].gender2);
        System.out.println("customerDetailsForLuxuryDoubleRoom");
    }

    @Test
    public void customerDetailsForDeluxeDoubleRoom() {
        Hotel.bookroom(2);
        assertEquals("Johnny", Hotel.hotel_ob.deluxe_doublerrom[1].name);
        assertEquals("123", Hotel.hotel_ob.deluxe_doublerrom[1].contact);
        assertEquals("male", Hotel.hotel_ob.deluxe_doublerrom[1].gender);
        assertEquals("Mike", Hotel.hotel_ob.deluxe_doublerrom[1].name2);
        assertEquals("1234", Hotel.hotel_ob.deluxe_doublerrom[1].contact2);
        assertEquals("male", Hotel.hotel_ob.deluxe_doublerrom[1].gender2);
        System.out.println("customerDetailsForDeluxeDoubleRoom");
    }

    @Test
    public void customerDetailsForLuxurySingleRoom() {
        Hotel.bookroom(3);
        assertEquals("Rahul", Hotel.hotel_ob.luxury_singleerrom[9].name);
        assertEquals("123", Hotel.hotel_ob.luxury_singleerrom[9].contact);
        assertEquals("male", Hotel.hotel_ob.luxury_singleerrom[9].gender);
        System.out.println("customerDetailsForLuxurySingleRoom");
    }

    @Test
    public void customerDetailsForDeluxeSingleRoom() {
        Hotel.bookroom(4);
        assertEquals("Shivani", Hotel.hotel_ob.luxury_singleerrom[9].name);
        assertEquals("1233", Hotel.hotel_ob.luxury_singleerrom[9].contact);
        assertEquals("female", Hotel.hotel_ob.luxury_singleerrom[9].gender);
        System.out.println("customerDetailsForDeluxeSingleRoom");
    }

    @Test
    public void availabilityOfAllTypeRooms() {
        Hotel.bookroom(1);
        Hotel.bookroom(2);
        Hotel.bookroom(3);
        Hotel.bookroom(4);
        assertEquals(9, Hotel.availability(1));
        assertEquals(19, Hotel.availability(2));
        assertEquals(9, Hotel.availability(3));
        assertEquals(19, Hotel.availability(4));

    }

    @Test
    public void deallocateRoom() {
        Hotel.bookroom(1); // allocating room in luxury_doublerrom
        Hotel.bookroom(2); // allocating room in deluxe_doublerrom
        Hotel.bookroom(3); // allocating room in luxury_singleerrom
        Hotel.bookroom(4); // allocating room in deluxe_singleerrom
        System.out.println(Hotel.hotel_ob.luxury_doublerrom[0].name);
        System.out.println(Hotel.hotel_ob.deluxe_doublerrom[0].name);
        System.out.println(Hotel.hotel_ob.luxury_singleerrom[0].name);
        System.out.println(Hotel.hotel_ob.deluxe_singleerrom[0].name);
        Hotel.deallocate(1 - 1, 1); //for room no.1
        Hotel.deallocate(11 - 11, 2); //for room no.11
        Hotel.deallocate(31 - 31, 3); //for room no.31
        Hotel.deallocate(41 - 41, 4); //for room no.41
        assertNull(Hotel.hotel_ob.luxury_doublerrom[1 - 1]);
        assertNull(Hotel.hotel_ob.deluxe_doublerrom[11 - 11]);
        assertNull(Hotel.hotel_ob.luxury_singleerrom[31 - 31]);
        assertNull(Hotel.hotel_ob.deluxe_singleerrom[41 - 41]);
    }

    @Test
    public void BillWithoutFood() {
        double amount = 4000;
        Hotel.bookroom(1);  // booking room
        for (Food food : Hotel.hotel_ob.luxury_doublerrom[0].food) {
            amount += food.price;
        }
        System.out.println(amount);
        assertEquals(4000, amount); // total amount for luxury_doublerrom without food.
    }

    @Test
    public void BillWithFood() {
        double amount = 4000;
        Hotel.bookroom(1);  // booking room
        Hotel.order(1 - 1, 1);//ordering one sandwich of price 50.
        for (Food food : Hotel.hotel_ob.luxury_doublerrom[0].food) {
            amount += food.price;
        }
        System.out.println(amount);
        assertEquals(4050, amount); // total amount for luxury_doublerrom with food.
    }
}




