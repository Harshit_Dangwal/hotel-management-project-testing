import org.junit.runner.*;
import org.junit.runner.notification.Failure;
import org.junit.runners.Suite;

public class Main {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(SuiteClasses.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println("Result=="+result.wasSuccessful());
    }
}
