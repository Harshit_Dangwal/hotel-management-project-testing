import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SingleroomTest {
    Singleroom customer;

    @Before
    public void createInstance() {
        customer = new Singleroom("henry", "1234567890", "male");
    }

    @Test
    public void singleRoomDetails() {
        assertEquals("henry", customer.name, "name did not match");
        assertEquals("1234567890", customer.contact, "contact did not match");
        assertEquals("male", customer.gender, "gender did not match");
        System.out.println("SingleroomTest");
    }

    @Test
    public void NotEmptyDetails() {
        assertFalse(customer.name.isEmpty());
        assertFalse(customer.contact.isEmpty());
        assertFalse(customer.gender.isEmpty());
    }

    @Test
    public void detailsShouldNotContainNull() {
        assertNotNull(customer.name);
        assertNotNull(customer.contact);
        assertNotNull(customer.gender);
    }
}