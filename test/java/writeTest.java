import org.junit.Assert;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

public class writeTest {
    @Test
    public void writeObjectFile() {
        assertNull(Hotel.hotel_ob.deluxe_doublerrom[0]); // before allocating room.
        Hotel.bookroom(1); //saving customer details for luxury_doublerrom.
        try {
            File f = new File("backup");
            if (f.exists()) {
                FileInputStream fin = new FileInputStream(f);
                ObjectInputStream ois = new ObjectInputStream(fin);
                Hotel.hotel_ob = (holder) ois.readObject();   // saving previous data in Hotel.hotel_ob object from backup file if any.
            }
        } catch (Exception e) {
            System.out.println("Not a valid input");
        }
        Thread t = new Thread(new write(Hotel.hotel_ob));
        t.start();
        Hotel.availability(1);  // checking for room availability if not in backup file.
        assertNotNull(Hotel.hotel_ob.luxury_doublerrom[0]); // after allocating room.
    }
}